import { IEspecialidad } from 'app/shared/model//especialidad.model';

export interface IAsesor {
    id?: number;
    nombre?: string;
    especialidades?: IEspecialidad;
}

export class Asesor implements IAsesor {
    constructor(public id?: number, public nombre?: string, public especialidades?: IEspecialidad) {}
}
