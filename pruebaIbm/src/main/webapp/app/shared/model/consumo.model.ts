import { Moment } from 'moment';

export interface IConsumo {
    id?: number;
    fecha?: Moment;
    descricion?: string;
    monto?: number;
}

export class Consumo implements IConsumo {
    constructor(public id?: number, public fecha?: Moment, public descricion?: string, public monto?: number) {}
}
