import { IConsumo } from 'app/shared/model//consumo.model';

export interface ITarjeta {
    id?: number;
    numero?: string;
    tipo?: string;
    ccv?: number;
    consumos?: IConsumo;
}

export class Tarjeta implements ITarjeta {
    constructor(public id?: number, public numero?: string, public tipo?: string, public ccv?: number, public consumos?: IConsumo) {}
}
