export interface IEspecialidad {
    id?: number;
    nombre?: string;
}

export class Especialidad implements IEspecialidad {
    constructor(public id?: number, public nombre?: string) {}
}
