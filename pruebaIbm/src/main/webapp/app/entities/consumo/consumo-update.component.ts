import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import * as moment from 'moment';
import { DATE_TIME_FORMAT } from 'app/shared/constants/input.constants';

import { IConsumo } from 'app/shared/model/consumo.model';
import { ConsumoService } from './consumo.service';

@Component({
    selector: 'jhi-consumo-update',
    templateUrl: './consumo-update.component.html'
})
export class ConsumoUpdateComponent implements OnInit {
    consumo: IConsumo;
    isSaving: boolean;
    fecha: string;

    constructor(private consumoService: ConsumoService, private activatedRoute: ActivatedRoute) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ consumo }) => {
            this.consumo = consumo;
            this.fecha = this.consumo.fecha != null ? this.consumo.fecha.format(DATE_TIME_FORMAT) : null;
        });
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        this.consumo.fecha = this.fecha != null ? moment(this.fecha, DATE_TIME_FORMAT) : null;
        if (this.consumo.id !== undefined) {
            this.subscribeToSaveResponse(this.consumoService.update(this.consumo));
        } else {
            this.subscribeToSaveResponse(this.consumoService.create(this.consumo));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IConsumo>>) {
        result.subscribe((res: HttpResponse<IConsumo>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }
}
