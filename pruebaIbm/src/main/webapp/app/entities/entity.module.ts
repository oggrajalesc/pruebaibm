import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';

import { PruebaIbmClienteModule } from './cliente/cliente.module';
import { PruebaIbmTarjetaModule } from './tarjeta/tarjeta.module';
import { PruebaIbmConsumoModule } from './consumo/consumo.module';
import { PruebaIbmAsesorModule } from './asesor/asesor.module';
import { PruebaIbmEspecialidadModule } from './especialidad/especialidad.module';
/* jhipster-needle-add-entity-module-import - JHipster will add entity modules imports here */

@NgModule({
    // prettier-ignore
    imports: [
        PruebaIbmClienteModule,
        PruebaIbmTarjetaModule,
        PruebaIbmConsumoModule,
        PruebaIbmAsesorModule,
        PruebaIbmEspecialidadModule,
        /* jhipster-needle-add-entity-module - JHipster will add entity modules here */
    ],
    declarations: [],
    entryComponents: [],
    providers: [],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PruebaIbmEntityModule {}
