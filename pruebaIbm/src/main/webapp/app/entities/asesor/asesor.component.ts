import { Component, OnInit, OnDestroy } from '@angular/core';
import { HttpErrorResponse, HttpResponse } from '@angular/common/http';
import { Subscription } from 'rxjs';
import { JhiEventManager, JhiAlertService } from 'ng-jhipster';

import { IAsesor } from 'app/shared/model/asesor.model';
import { Principal } from 'app/core';
import { AsesorService } from './asesor.service';

@Component({
    selector: 'jhi-asesor',
    templateUrl: './asesor.component.html'
})
export class AsesorComponent implements OnInit, OnDestroy {
    asesors: IAsesor[];
    currentAccount: any;
    eventSubscriber: Subscription;

    constructor(
        private asesorService: AsesorService,
        private jhiAlertService: JhiAlertService,
        private eventManager: JhiEventManager,
        private principal: Principal
    ) {}

    loadAll() {
        this.asesorService.query().subscribe(
            (res: HttpResponse<IAsesor[]>) => {
                this.asesors = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    ngOnInit() {
        this.loadAll();
        this.principal.identity().then(account => {
            this.currentAccount = account;
        });
        this.registerChangeInAsesors();
    }

    ngOnDestroy() {
        this.eventManager.destroy(this.eventSubscriber);
    }

    trackId(index: number, item: IAsesor) {
        return item.id;
    }

    registerChangeInAsesors() {
        this.eventSubscriber = this.eventManager.subscribe('asesorListModification', response => this.loadAll());
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }
}
