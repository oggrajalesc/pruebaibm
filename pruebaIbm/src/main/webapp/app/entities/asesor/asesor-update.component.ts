import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { IAsesor } from 'app/shared/model/asesor.model';
import { AsesorService } from './asesor.service';
import { IEspecialidad } from 'app/shared/model/especialidad.model';
import { EspecialidadService } from 'app/entities/especialidad';

@Component({
    selector: 'jhi-asesor-update',
    templateUrl: './asesor-update.component.html'
})
export class AsesorUpdateComponent implements OnInit {
    asesor: IAsesor;
    isSaving: boolean;

    especialidads: IEspecialidad[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private asesorService: AsesorService,
        private especialidadService: EspecialidadService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ asesor }) => {
            this.asesor = asesor;
        });
        this.especialidadService.query().subscribe(
            (res: HttpResponse<IEspecialidad[]>) => {
                this.especialidads = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.asesor.id !== undefined) {
            this.subscribeToSaveResponse(this.asesorService.update(this.asesor));
        } else {
            this.subscribeToSaveResponse(this.asesorService.create(this.asesor));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<IAsesor>>) {
        result.subscribe((res: HttpResponse<IAsesor>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackEspecialidadById(index: number, item: IEspecialidad) {
        return item.id;
    }
}
