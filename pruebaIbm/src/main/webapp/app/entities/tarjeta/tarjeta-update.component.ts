import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { JhiAlertService } from 'ng-jhipster';

import { ITarjeta } from 'app/shared/model/tarjeta.model';
import { TarjetaService } from './tarjeta.service';
import { IConsumo } from 'app/shared/model/consumo.model';
import { ConsumoService } from 'app/entities/consumo';

@Component({
    selector: 'jhi-tarjeta-update',
    templateUrl: './tarjeta-update.component.html'
})
export class TarjetaUpdateComponent implements OnInit {
    tarjeta: ITarjeta;
    isSaving: boolean;

    consumos: IConsumo[];

    constructor(
        private jhiAlertService: JhiAlertService,
        private tarjetaService: TarjetaService,
        private consumoService: ConsumoService,
        private activatedRoute: ActivatedRoute
    ) {}

    ngOnInit() {
        this.isSaving = false;
        this.activatedRoute.data.subscribe(({ tarjeta }) => {
            this.tarjeta = tarjeta;
        });
        this.consumoService.query().subscribe(
            (res: HttpResponse<IConsumo[]>) => {
                this.consumos = res.body;
            },
            (res: HttpErrorResponse) => this.onError(res.message)
        );
    }

    previousState() {
        window.history.back();
    }

    save() {
        this.isSaving = true;
        if (this.tarjeta.id !== undefined) {
            this.subscribeToSaveResponse(this.tarjetaService.update(this.tarjeta));
        } else {
            this.subscribeToSaveResponse(this.tarjetaService.create(this.tarjeta));
        }
    }

    private subscribeToSaveResponse(result: Observable<HttpResponse<ITarjeta>>) {
        result.subscribe((res: HttpResponse<ITarjeta>) => this.onSaveSuccess(), (res: HttpErrorResponse) => this.onSaveError());
    }

    private onSaveSuccess() {
        this.isSaving = false;
        this.previousState();
    }

    private onSaveError() {
        this.isSaving = false;
    }

    private onError(errorMessage: string) {
        this.jhiAlertService.error(errorMessage, null, null);
    }

    trackConsumoById(index: number, item: IConsumo) {
        return item.id;
    }
}
