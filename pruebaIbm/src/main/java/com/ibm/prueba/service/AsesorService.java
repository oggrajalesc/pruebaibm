package com.ibm.prueba.service;

import com.ibm.prueba.domain.Asesor;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Asesor.
 */
public interface AsesorService {

    /**
     * Save a asesor.
     *
     * @param asesor the entity to save
     * @return the persisted entity
     */
    Asesor save(Asesor asesor);

    /**
     * Get all the asesors.
     *
     * @return the list of entities
     */
    List<Asesor> findAll();


    /**
     * Get the "id" asesor.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Asesor> findOne(Long id);

    /**
     * Delete the "id" asesor.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
