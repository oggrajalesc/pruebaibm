package com.ibm.prueba.service;

import com.ibm.prueba.domain.Consumo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Consumo.
 */
public interface ConsumoService {

    /**
     * Save a consumo.
     *
     * @param consumo the entity to save
     * @return the persisted entity
     */
    Consumo save(Consumo consumo);

    /**
     * Get all the consumos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Consumo> findAll(Pageable pageable);


    /**
     * Get the "id" consumo.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Consumo> findOne(Long id);

    /**
     * Delete the "id" consumo.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
