package com.ibm.prueba.service.impl;

import com.ibm.prueba.service.AsesorService;
import com.ibm.prueba.domain.Asesor;
import com.ibm.prueba.repository.AsesorRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing Asesor.
 */
@Service
@Transactional
public class AsesorServiceImpl implements AsesorService {

    private final Logger log = LoggerFactory.getLogger(AsesorServiceImpl.class);

    private final AsesorRepository asesorRepository;

    public AsesorServiceImpl(AsesorRepository asesorRepository) {
        this.asesorRepository = asesorRepository;
    }

    /**
     * Save a asesor.
     *
     * @param asesor the entity to save
     * @return the persisted entity
     */
    @Override
    public Asesor save(Asesor asesor) {
        log.debug("Request to save Asesor : {}", asesor);
        return asesorRepository.save(asesor);
    }

    /**
     * Get all the asesors.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Asesor> findAll() {
        log.debug("Request to get all Asesors");
        return asesorRepository.findAll();
    }


    /**
     * Get one asesor by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Asesor> findOne(Long id) {
        log.debug("Request to get Asesor : {}", id);
        return asesorRepository.findById(id);
    }

    /**
     * Delete the asesor by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Asesor : {}", id);
        asesorRepository.deleteById(id);
    }
}
