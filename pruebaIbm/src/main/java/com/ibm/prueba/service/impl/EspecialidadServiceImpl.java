package com.ibm.prueba.service.impl;

import com.ibm.prueba.service.EspecialidadService;
import com.ibm.prueba.domain.Especialidad;
import com.ibm.prueba.repository.EspecialidadRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

/**
 * Service Implementation for managing Especialidad.
 */
@Service
@Transactional
public class EspecialidadServiceImpl implements EspecialidadService {

    private final Logger log = LoggerFactory.getLogger(EspecialidadServiceImpl.class);

    private final EspecialidadRepository especialidadRepository;

    public EspecialidadServiceImpl(EspecialidadRepository especialidadRepository) {
        this.especialidadRepository = especialidadRepository;
    }

    /**
     * Save a especialidad.
     *
     * @param especialidad the entity to save
     * @return the persisted entity
     */
    @Override
    public Especialidad save(Especialidad especialidad) {
        log.debug("Request to save Especialidad : {}", especialidad);
        return especialidadRepository.save(especialidad);
    }

    /**
     * Get all the especialidads.
     *
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public List<Especialidad> findAll() {
        log.debug("Request to get all Especialidads");
        return especialidadRepository.findAll();
    }


    /**
     * Get one especialidad by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Especialidad> findOne(Long id) {
        log.debug("Request to get Especialidad : {}", id);
        return especialidadRepository.findById(id);
    }

    /**
     * Delete the especialidad by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Especialidad : {}", id);
        especialidadRepository.deleteById(id);
    }
}
