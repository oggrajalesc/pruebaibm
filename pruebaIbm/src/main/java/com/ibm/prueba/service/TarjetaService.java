package com.ibm.prueba.service;

import com.ibm.prueba.domain.Tarjeta;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * Service Interface for managing Tarjeta.
 */
public interface TarjetaService {

    /**
     * Save a tarjeta.
     *
     * @param tarjeta the entity to save
     * @return the persisted entity
     */
    Tarjeta save(Tarjeta tarjeta);

    /**
     * Get all the tarjetas.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    Page<Tarjeta> findAll(Pageable pageable);


    /**
     * Get the "id" tarjeta.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Tarjeta> findOne(Long id);

    /**
     * Delete the "id" tarjeta.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
