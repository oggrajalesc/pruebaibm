package com.ibm.prueba.service.impl;

import com.ibm.prueba.service.ConsumoService;
import com.ibm.prueba.domain.Consumo;
import com.ibm.prueba.repository.ConsumoRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

/**
 * Service Implementation for managing Consumo.
 */
@Service
@Transactional
public class ConsumoServiceImpl implements ConsumoService {

    private final Logger log = LoggerFactory.getLogger(ConsumoServiceImpl.class);

    private final ConsumoRepository consumoRepository;

    public ConsumoServiceImpl(ConsumoRepository consumoRepository) {
        this.consumoRepository = consumoRepository;
    }

    /**
     * Save a consumo.
     *
     * @param consumo the entity to save
     * @return the persisted entity
     */
    @Override
    public Consumo save(Consumo consumo) {
        log.debug("Request to save Consumo : {}", consumo);
        return consumoRepository.save(consumo);
    }

    /**
     * Get all the consumos.
     *
     * @param pageable the pagination information
     * @return the list of entities
     */
    @Override
    @Transactional(readOnly = true)
    public Page<Consumo> findAll(Pageable pageable) {
        log.debug("Request to get all Consumos");
        return consumoRepository.findAll(pageable);
    }


    /**
     * Get one consumo by id.
     *
     * @param id the id of the entity
     * @return the entity
     */
    @Override
    @Transactional(readOnly = true)
    public Optional<Consumo> findOne(Long id) {
        log.debug("Request to get Consumo : {}", id);
        return consumoRepository.findById(id);
    }

    /**
     * Delete the consumo by id.
     *
     * @param id the id of the entity
     */
    @Override
    public void delete(Long id) {
        log.debug("Request to delete Consumo : {}", id);
        consumoRepository.deleteById(id);
    }
}
