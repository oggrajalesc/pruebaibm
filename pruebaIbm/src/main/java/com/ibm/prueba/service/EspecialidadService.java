package com.ibm.prueba.service;

import com.ibm.prueba.domain.Especialidad;

import java.util.List;
import java.util.Optional;

/**
 * Service Interface for managing Especialidad.
 */
public interface EspecialidadService {

    /**
     * Save a especialidad.
     *
     * @param especialidad the entity to save
     * @return the persisted entity
     */
    Especialidad save(Especialidad especialidad);

    /**
     * Get all the especialidads.
     *
     * @return the list of entities
     */
    List<Especialidad> findAll();


    /**
     * Get the "id" especialidad.
     *
     * @param id the id of the entity
     * @return the entity
     */
    Optional<Especialidad> findOne(Long id);

    /**
     * Delete the "id" especialidad.
     *
     * @param id the id of the entity
     */
    void delete(Long id);
}
