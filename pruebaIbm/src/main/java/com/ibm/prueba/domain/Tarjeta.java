package com.ibm.prueba.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Objects;

/**
 * A Tarjeta.
 */
@Entity
@Table(name = "tarjeta")
public class Tarjeta implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "numero", nullable = false)
    private String numero;

    @NotNull
    @Column(name = "tipo", nullable = false)
    private String tipo;

    @Column(name = "ccv")
    private Long ccv;

    @ManyToOne
    @JsonIgnoreProperties("")
    private Consumo consumos;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNumero() {
        return numero;
    }

    public Tarjeta numero(String numero) {
        this.numero = numero;
        return this;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getTipo() {
        return tipo;
    }

    public Tarjeta tipo(String tipo) {
        this.tipo = tipo;
        return this;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Long getCcv() {
        return ccv;
    }

    public Tarjeta ccv(Long ccv) {
        this.ccv = ccv;
        return this;
    }

    public void setCcv(Long ccv) {
        this.ccv = ccv;
    }

    public Consumo getConsumos() {
        return consumos;
    }

    public Tarjeta consumos(Consumo consumo) {
        this.consumos = consumo;
        return this;
    }

    public void setConsumos(Consumo consumo) {
        this.consumos = consumo;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Tarjeta tarjeta = (Tarjeta) o;
        if (tarjeta.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), tarjeta.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Tarjeta{" +
            "id=" + getId() +
            ", numero='" + getNumero() + "'" +
            ", tipo='" + getTipo() + "'" +
            ", ccv=" + getCcv() +
            "}";
    }
}
