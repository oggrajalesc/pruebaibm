package com.ibm.prueba.domain;

import io.swagger.annotations.ApiModel;

import javax.persistence.*;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;
import java.util.Objects;

/**
 * not an ignored comment
 */
@ApiModel(description = "not an ignored comment")
@Entity
@Table(name = "consumo")
public class Consumo implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "fecha", nullable = false)
    private Instant fecha;

    @Column(name = "descricion")
    private String descricion;

    @NotNull
    @Column(name = "monto", precision = 10, scale = 2, nullable = false)
    private BigDecimal monto;

    // jhipster-needle-entity-add-field - JHipster will add fields here, do not remove
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getFecha() {
        return fecha;
    }

    public Consumo fecha(Instant fecha) {
        this.fecha = fecha;
        return this;
    }

    public void setFecha(Instant fecha) {
        this.fecha = fecha;
    }

    public String getDescricion() {
        return descricion;
    }

    public Consumo descricion(String descricion) {
        this.descricion = descricion;
        return this;
    }

    public void setDescricion(String descricion) {
        this.descricion = descricion;
    }

    public BigDecimal getMonto() {
        return monto;
    }

    public Consumo monto(BigDecimal monto) {
        this.monto = monto;
        return this;
    }

    public void setMonto(BigDecimal monto) {
        this.monto = monto;
    }
    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here, do not remove

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Consumo consumo = (Consumo) o;
        if (consumo.getId() == null || getId() == null) {
            return false;
        }
        return Objects.equals(getId(), consumo.getId());
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(getId());
    }

    @Override
    public String toString() {
        return "Consumo{" +
            "id=" + getId() +
            ", fecha='" + getFecha() + "'" +
            ", descricion='" + getDescricion() + "'" +
            ", monto=" + getMonto() +
            "}";
    }
}
