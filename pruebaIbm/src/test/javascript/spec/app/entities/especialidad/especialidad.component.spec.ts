/* tslint:disable max-line-length */
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Observable, of } from 'rxjs';
import { HttpHeaders, HttpResponse } from '@angular/common/http';

import { PruebaIbmTestModule } from '../../../test.module';
import { EspecialidadComponent } from 'app/entities/especialidad/especialidad.component';
import { EspecialidadService } from 'app/entities/especialidad/especialidad.service';
import { Especialidad } from 'app/shared/model/especialidad.model';

describe('Component Tests', () => {
    describe('Especialidad Management Component', () => {
        let comp: EspecialidadComponent;
        let fixture: ComponentFixture<EspecialidadComponent>;
        let service: EspecialidadService;

        beforeEach(() => {
            TestBed.configureTestingModule({
                imports: [PruebaIbmTestModule],
                declarations: [EspecialidadComponent],
                providers: []
            })
                .overrideTemplate(EspecialidadComponent, '')
                .compileComponents();

            fixture = TestBed.createComponent(EspecialidadComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(EspecialidadService);
        });

        it('Should call load all on init', () => {
            // GIVEN
            const headers = new HttpHeaders().append('link', 'link;link');
            spyOn(service, 'query').and.returnValue(
                of(
                    new HttpResponse({
                        body: [new Especialidad(123)],
                        headers
                    })
                )
            );

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.query).toHaveBeenCalled();
            expect(comp.especialidads[0]).toEqual(jasmine.objectContaining({ id: 123 }));
        });
    });
});
